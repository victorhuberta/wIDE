package application;

import java.io.File;
import java.nio.file.Paths;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;

public class Resources {
    public static ScrollPane scrollPane = new ScrollPane();
	public static Tab resourcesTab = new Tab();
	public static GridPane gridPane = new GridPane();
	public static final String[] imgExtensions = {"jpeg", "jpg", "png", "gif", "bmp"};
	
	public static void loadContent() {
		/*
		 * Load all images in the static/ folder
		 */
		String path = Paths.get(".").toAbsolutePath().normalize().toString() + "/src/res/static";
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
	
	    
        int col = 0;
        for (File file: listOfFiles) {
        	for (String ext: imgExtensions) {
        		if (file.getName().endsWith("." + ext)) {
        			/*
        			 * Filter images from all the files
        			 */
        			String imgPath = "res/static/" + file.getName();
        			Image img = new Image(imgPath);
                    ImageView imgView = new ImageView();
                    imgView.setImage(img);
                    imgView.setFitWidth(160);
                    imgView.setPreserveRatio(true);
                    imgView.setSmooth(true);
                    imgView.setCache(true);
                    imgView.setOnDragDetected(e -> {
                        /* drag was detected, start drag-and-drop gesture*/
                        /* allow COPY transfer mode */
                        Dragboard db = imgView.startDragAndDrop(TransferMode.COPY);
                        /* set the image on drag */
                        db.setDragView(imgView.getImage(), e.getX(), e.getY());
                        
                        /* put a string on dragboard */
                        ClipboardContent content = new ClipboardContent();
                        content.putString(imgPath);
                    
                        db.setContent(content);
                        
                        e.consume();
        			});             
                    
                    imgView.setOnDragDone(e -> {
                    	e.consume();
                    });
                    
                    GridPane.setConstraints(imgView, col++, 0);
                    gridPane.getChildren().add(imgView);
                    break;
        		}
        	}
        }
        scrollPane.setContent(gridPane);
        gridPane.prefHeightProperty().bind(TabCollections.utilityTabs.heightProperty().multiply(0.89));
        gridPane.prefWidthProperty().bind(TabCollections.utilityTabs.widthProperty().multiply(0.97));
        gridPane.setId("console-area");
        
	}

	public static void settings() {
		resourcesTab.setClosable(false);
		resourcesTab.setText("Resources");
		resourcesTab.setContent(scrollPane);
		resourcesTab.setId("editor-tab");
		loadContent();
	}
}
