package application;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class Emulation {
	public static HBox emulationBar = new HBox(10);
	public static Label emulationLabel = new Label("");
	public static ComboBox<String> emulationComboBox = new ComboBox<>();
	public static TextField emulationPromptText = new TextField();
	private static ImageView arrowImage = new ImageView("res/images/surf_button2.png");
	public static Label emulationButton = new Label("", arrowImage);
	public static HashMap<String, ImageView> browser_imgMap = new HashMap<>();
	
	public static void settings() {
		emulationBar.setPadding(new Insets(4, 0, 0, 5));
		emulationBar.setPrefHeight(50);
		emulationPromptText.setMinWidth(400);
		
		String[] img_paths = {"res/images/chrome.png", "res/images/firefox.png", 
				"res/images/ie.png", "res/images/safari.png"};
		
		emulationButton.setId("surf-button");
		emulationPromptText.setId("address-bar");
		
		for (String img_path : img_paths) {
			Image img = new Image(img_path);
			ImageView imgView = new ImageView();
			imgView.setImage(img);
	        imgView.setFitWidth(20);
	        imgView.setPreserveRatio(true);
	        imgView.setSmooth(true);
	        imgView.setCache(true);
	        if (img_path.equals("res/images/chrome.png")) {
	        	browser_imgMap.put("Google Chrome", imgView);
	        } else if (img_path.equals("res/images/firefox.png")) {
	        	browser_imgMap.put("Mozilla Firefox", imgView);
	        } else if (img_path.equals("res/images/ie.png")) {
	        	browser_imgMap.put("Internet Explorer", imgView);
	        } else if (img_path.equals("res/images/safari.png")) {
	        	browser_imgMap.put("Safari", imgView);
	        }
		}
		
		Iterator<Map.Entry<String, ImageView>> it = browser_imgMap.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry<String, ImageView> entry = (Map.Entry<String, ImageView>) it.next();
			emulationComboBox.getItems().add(entry.getKey());
		}
		
		emulationLabel.setGraphic(browser_imgMap.get("Google Chrome"));
		emulationComboBox.setValue("Google Chrome");
		emulationPromptText.setPromptText("Address Bar");
		
		emulationBar.getChildren().addAll(emulationLabel, emulationComboBox, emulationPromptText, emulationButton);
	}
}
