package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Console {
	public static VBox consoleBox = new VBox(10);
	public static HBox consolePromptBox = new HBox(10);
	public static Tab consoleTab = new Tab();
	public static TextArea consoleText = new TextArea();
	public static TextField consolePromptText = new TextField();
	public static Label consolePromptLabel = new Label("Command:");
	public static Button consoleRunButton = new Button("Run");
	
	public static void settings() { 	
		consoleTab.setClosable(false);
		consoleTab.setText("Console");
		consoleTab.setContent(consoleBox);
	
		consoleText.setId("console-area");
		
		consoleText.setEditable(false);
		consoleText.setWrapText(false);
		consolePromptText.setPromptText("write a command");
		consolePromptLabel.setPadding(new Insets(4, 0, 0, 5));
		consolePromptText.setMinWidth(400);
		
		consolePromptText.setId("console-text-field");
		consoleRunButton.setId("console-run-button");
		consoleTab.setId("editor-tab");
		
		consolePromptBox.getChildren().addAll(consolePromptLabel, consolePromptText,
				consoleRunButton);
		consoleBox.getChildren().addAll(consoleText, consolePromptBox);
	}
	
	public static void processConsole() {
		String cmd = consolePromptText.getText();
		if (cmd.isEmpty()) {
			return;
		}
		String output = runCommand(cmd);
		consoleText.appendText("\n>> " + cmd + "\n" );
		consoleText.appendText(output);
		consolePromptText.clear();
	}
	
	public static String runCommand(String cmd) {
		Runtime rt = Runtime.getRuntime();
		String output = "";
		
		try {
			Process p = rt.exec(cmd);
			BufferedReader stdOutput = new BufferedReader(
							new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(
					new InputStreamReader(p.getErrorStream()));
			
			String s = null;
			
			while ((s = stdOutput.readLine()) != null) {
				output += s + "\n";
			}
			String e = null;
			while((e = stdError.readLine()) != null) {
				output += e + "\n";
			}
			
		} catch (IOException ioe) {
			output += "Command '" + cmd + "' not found.\n";
		}
		
		
		return output;
	}
}
