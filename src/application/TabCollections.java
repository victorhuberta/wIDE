package application;

import java.io.File;

import javafx.collections.ObservableList;
import javafx.scene.control.Tab;	
import javafx.scene.control.TabPane;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import org.fxmisc.richtext.*;
import codeeditor.JavaSyntax;

public class TabCollections {
	public static TabPane editorTabs = new TabPane();
	public static TabPane utilityTabs = new TabPane();
	
	public static void settings() {
		utilityTabs.prefHeightProperty().bind(Main.window.heightProperty().multiply(0.3));
		editorTabs.prefHeightProperty().bind(Main.window.heightProperty().multiply(0.65));
		utilityTabs.getTabs().addAll(Console.consoleTab, Resources.resourcesTab);
	}
	
	public static Tab addNewTab(String title, String content) {
		Tab tab = new Tab(title);
		tab.setId("editor-tab");
		
		CodeArea editorText = new CodeArea();
		editorText.setParagraphGraphicFactory(LineNumberFactory.get(editorText));
		editorText.richChanges().subscribe(change -> {
            editorText.setStyleSpans(0, JavaSyntax.computeHighlighting(editorText.getText()));
        });
        editorText.replaceText(0, 0, content);
        
		tab.setContent(editorText);
		setEditorTextOnDragEvents(editorText);
		editorText.getStylesheets().add("codeeditor/code-editor.css");
		editorTabs.getTabs().add(tab);
		editorTabs.getSelectionModel().select(tab);
		return tab;
	}
	
	public static void setEditorTextOnDragEvents(CodeArea target) {
		/*
		 * Set all the appropriate DRAG events on CodeArea
		 */
		
		target.setOnDragOver(e -> {
        	/* accept it only if it is  not dragged from the same node 
             * and if it has a string data */
        	if (e.getGestureSource() != target &&
                    e.getDragboard().hasString()) {
                /* allow for both copying and moving, whatever user chooses */
                e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            e.consume();
        });
        
        target.setOnDragEntered(e -> {
    		target.setId("editor-text");
        	// Highlight the CodeArea upon dragEntered
			target.getStylesheets().add("codeeditor/highlighter.css");
            e.consume();
        });
        
        target.setOnDragExited(e -> {
        	// Remove highlight
        	target.getStylesheets().remove("codeeditor/highlighter.css");
        	e.consume();
        });
        
        target.setOnDragDropped(e -> {
        	 // Insert <img> tag to the CodeArea
        	 Dragboard db = e.getDragboard();
             boolean success = false;
             
             String output = "";
             
             if (db.hasString()) {
            	 String dbString = db.getString();
            	 if (isImage(dbString)) {
            		 output = dbString;
            	 }
             }
             
             if (db.hasUrl()) {
            	 String dbUrl = db.getUrl();
            	 if (isImage(dbUrl)) {
            		 output = dbUrl;
            	 }
             }
    		 
             if (!output.equals("")) {
    			 output = "<img src=\"" + output + "\"/>";
        		 target.insertText(target.getCaretPosition(), output + "\n");
                 success = true;
             }
             
             /* let the source know whether the string was successfully 
              * transferred and used */
             e.setDropCompleted(success);
             
             e.consume();
        });
	}
	
	public static boolean isImage(String str) {
		 boolean isImage = false;
		 for (String ext: Resources.imgExtensions) {
			 if (str.endsWith(ext)){
				 isImage = true;
			 }
		 }
		 return isImage;
	}
	
	public static void openFile() {
		File file = FileProcessor.chooseFile(Main.window, "Open File");
		if (!file.exists()) {
			return;
		}
		
		ObservableList<Tab> tablist = editorTabs.getTabs();
		
		for (Tab tab : tablist) {
			if (tab.getText().equals(file.getName())) {
				editorTabs.getSelectionModel().select(tab);
				return;
			}
		}
		String content = FileProcessor.processFile(file);
		if (!file.getName().isEmpty())
			addNewTab(file.getName(), content);
		else
			addNewTab("Empty", content);
	}
	
	public static void saveFile(Tab selectedTab, boolean saveAs) {
		String filename = selectedTab.getText();
		CodeArea editorText = (CodeArea) selectedTab.getContent();
		String content = editorText.getText();
		File saveFile = FileProcessor.saveFile(Main.window, filename, content, saveAs);
		if (saveFile != null) {
			selectedTab.setText(saveFile.getName());
		}
		Browser.engine.reload();
	}
	
	public static void saveAllFiles() {
		ObservableList<Tab> tablist = editorTabs.getTabs();
		if (tablist != null) {
			for (Tab tab : tablist) {
				saveFile(tab, tab.getText().equalsIgnoreCase("Empty"));
			}
		}
	}
	
	public static void closeFile(boolean all) {
		if (all) {
			editorTabs.getTabs().removeAll(editorTabs.getTabs());
			FileProcessor.removeFromFiles("", all);
			return;
		}
		Tab selectedTab = editorTabs.getSelectionModel().getSelectedItem();
		if (selectedTab != null) {
			String filename = selectedTab.getText();
			editorTabs.getTabs().remove(selectedTab);
			FileProcessor.removeFromFiles(filename, all);
		}
	}
}
