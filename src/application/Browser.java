package application;

import java.io.File;
import java.io.IOException;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class Browser {
	public static WebView browser = new WebView();
	public static WebEngine engine = browser.getEngine();
	
	public static void settings() {
		engine.load("https://www.google.com.my/maps/");
		//engine.load("http://[::1]:8000/homepage.php");
		browser.prefWidthProperty().bind(Main.window.widthProperty().multiply(0.55));
		browser.prefHeightProperty().bind(Main.window.heightProperty().subtract(Emulation.emulationBar.getPrefHeight()));
	}
	
	public static void loadScript() {
		/*
		 * Load any URL found in emulationPromptText on the browser
		 */
		String url = Emulation.emulationPromptText.getText();
		if (url.isEmpty())
			return;
		
		// If URL is directed to a file
		if (url.startsWith("file://")) {
			engine.load(url);
			return;
		}
		
		File htmlFile = new File(url);
		// If URL is directed to a file
		if (htmlFile.exists()) {
			try {
				engine.load("file://" + htmlFile.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		// If URL is not directed to a file
		} else {
			if (!url.startsWith("http://") && !url.startsWith("https://"))
				url = "http://" + url;
			/*String[] url_segments = url.split(":");
			String host = url_segments[0];*/
			engine.load(url);	
		}
	}
}
