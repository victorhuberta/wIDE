package application;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class FileProcessor {
	private static ArrayList<File> files = new ArrayList<>();
	
	public FileProcessor() {}
	
	/*public static File chooseFile(String desc, String ext) {
		FileChooser chooser = new FileChooser();
		if (!desc.isEmpty() || !ext.isEmpty()) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
					desc, ext);
			chooser.setFileFilter(filter);
		}
		int result = chooser.showOpenDialog(null);
		if (result == FileChooser.APPROVE_OPTION) {
			File selectedFile = chooser.getSelectedFile();
			files.add(selectedFile);
			return selectedFile;
		} else
			return new File("");
	}*/
	
	public static File chooseFile(Stage stage, String title) {
		FileChooser chooser = new FileChooser();
		chooser.setTitle(title);
		File selectedFile = chooser.showOpenDialog(stage);
		if (selectedFile != null) {
			files.add(selectedFile);
			return selectedFile;
		} else
			return new File("");
	}
	
	public static String processFile(File file) {
		String content = "";
		try {
			FileReader fReader = new FileReader(file);
			int chr = 0;
			while ((chr = fReader.read()) != -1) {
				content += (char) chr; 
			}
			fReader.close();
			return content;
		} catch (IOException ioe) {
			System.out.println("IOException: " + ioe.getMessage());
		} catch (SecurityException se) {
			System.out.println("SecurityException: " + se.getMessage());
		}
		return null;
	}
	
	public static File saveFile(Stage stage, String filename, String content, boolean saveAs) {
		File saveFile = new File("");
		FileWriter fw;
		
		for (File file : files) {
			if (file.getName().equals(filename)) {
				saveFile = file;
			}
		}
		
		if (saveAs) {
			FileChooser chooser = new FileChooser();
			chooser.setTitle("Save File for '" + filename + "'");
			saveFile = chooser.showSaveDialog(stage);
			if (saveFile != null) {
				files.add(saveFile);
			}
		} 
		
		try {
			fw = new FileWriter(saveFile);
			fw.write(content);
			fw.close();
			return saveFile;
		} catch (NullPointerException npe) {
		} catch (IOException ioe) {
			System.out.println("IOException: " + ioe.getMessage());
		}
		
		return null;
	}
	
	public static void removeFromFiles(String filename, boolean all) {
		if (all) {
			files.clear();
			return;
		}
		for (File file : files) {
			if (file.getName().equals(filename)) {
				files.remove(file);
				return;
			}
		}
	}
	
	public static String getFilePath(String filename) {
		try {
			for (File file: files) {
				if (file.getName().equals(filename)) {
					return file.getCanonicalPath();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "";
	}

}

