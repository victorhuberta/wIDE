package application;

import javafx.scene.control.Tab;
import javafx.scene.input.KeyCode;


public class WorkspaceEvent {
	public static void start() {
		/*browser.setOnMouseMoved(e -> {
			if (e.getSceneY() >= (0.95 * browser.getPrefHeight())) {
				emulationBar.setVisible(true);
				browser.prefHeightProperty().bind(window.heightProperty().multiply(0.95));
			} else {
				emulationBar.setVisible(false);
				browser.prefHeightProperty().bind(window.heightProperty());
			}
		});*/
		Browser.engine.locationProperty().addListener(e -> {
			Emulation.emulationPromptText.setText(Browser.engine.getLocation());
		});
		Emulation.emulationComboBox.setOnAction(e -> {
			Emulation.emulationLabel.setGraphic(
					Emulation.browser_imgMap.get(Emulation.emulationComboBox.getSelectionModel().getSelectedItem()));
		});
		Emulation.emulationPromptText.setOnKeyReleased(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				Browser.loadScript();
			}
		});
		Emulation.emulationButton.setOnMouseClicked(e -> {
			Browser.loadScript();
		});
		Console.consolePromptText.setOnKeyReleased(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				Console.processConsole();
			}
		});
		Console.consoleRunButton.setOnAction(e -> {
			Console.processConsole();
		});
		WorkspaceMenu.newMenuItem.setOnAction(e -> {
			TabCollections.addNewTab("Empty", "");
		});
		WorkspaceMenu.openMenuItem.setOnAction(e -> {
			TabCollections.openFile();
		});
		WorkspaceMenu.saveMenuItem.setOnAction(e -> {
			Tab selectedTab = TabCollections.editorTabs.getSelectionModel().getSelectedItem();
			if (selectedTab != null)
				TabCollections.saveFile(selectedTab, selectedTab.getText().equalsIgnoreCase("Empty"));
		});
		WorkspaceMenu.saveAsMenuItem.setOnAction(e -> {
			Tab selectedTab = TabCollections.editorTabs.getSelectionModel().getSelectedItem();
			if (selectedTab != null)
				TabCollections.saveFile(selectedTab, true);
		});
		WorkspaceMenu.saveAllMenuItem.setOnAction(e -> {
			TabCollections.saveAllFiles();
		});
		WorkspaceMenu.closeMenuItem.setOnAction(e -> {
			TabCollections.closeFile(false);
		});
		WorkspaceMenu.closeAllMenuItem.setOnAction(e -> {
			TabCollections.closeFile(true);
		});
		WorkspaceMenu.exitMenuItem.setOnAction(e -> Main.window.close());
		
		WorkspaceMenu.loadHTMLMenuItem.setOnAction(e -> {
			Tab selectedTab = TabCollections.editorTabs.getSelectionModel().getSelectedItem();
			if (selectedTab != null) {
				if (selectedTab.getText().endsWith(".html")) {
					Emulation.emulationPromptText.setText(FileProcessor.getFilePath(selectedTab.getText()));
					Browser.loadScript();
				}
			}
		});
		
	}
}
