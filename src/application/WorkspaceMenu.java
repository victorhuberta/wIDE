package application;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class WorkspaceMenu {
	public static MenuBar mainMenuBar = new MenuBar();
	public static Menu fileMenu = new Menu("_File");
	public static MenuItem newMenuItem = new MenuItem("New");
	public static MenuItem openMenuItem = new MenuItem("Open File..");
	public static MenuItem closeMenuItem = new MenuItem("Close");
	public static MenuItem closeAllMenuItem = new MenuItem("Close All");
	public static MenuItem saveMenuItem = new MenuItem("Save");
	public static MenuItem saveAsMenuItem = new MenuItem("Save As...");
	public static MenuItem saveAllMenuItem = new MenuItem("Save All");
	public static MenuItem settingsMenuItem = new MenuItem("Settings...");
	public static MenuItem exitMenuItem = new MenuItem("Exit");
	public static Menu toolsMenu = new Menu("_Tools");
	public static MenuItem loadHTMLMenuItem = new MenuItem("Load HTML");
	public static Menu helpMenu = new Menu("_Help");
	public static MenuItem tutorialMenuItem = new MenuItem("Tutorial");
	public static MenuItem aboutMenuItem = new MenuItem("About");
	
	public static void settings() {
		/*
		 * Setting up all the keyboard shortcuts
		 */
		saveMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
		loadHTMLMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN));
		
		fileMenu.getItems().addAll(newMenuItem, openMenuItem, closeMenuItem,
				closeAllMenuItem, new SeparatorMenuItem(),
				saveMenuItem, saveAsMenuItem, saveAllMenuItem, 
				new SeparatorMenuItem(), settingsMenuItem, 
				new SeparatorMenuItem(), exitMenuItem);
		toolsMenu.getItems().addAll(loadHTMLMenuItem);
		helpMenu.getItems().addAll(tutorialMenuItem, aboutMenuItem);
		mainMenuBar.getMenus().addAll(fileMenu, toolsMenu, helpMenu);
	}
}
