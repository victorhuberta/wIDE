package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Main extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public static Stage window;
	public static Scene scene;
	public static BorderPane mainLayout = new BorderPane();
	public static VBox viewLayout = new VBox();
	public static VBox editorLayout = new VBox();

	@Override
	public void start(Stage primaryStage) {
			// Window setup
			window = primaryStage;
			window.setTitle("WebIDE - workspace");
			window.setFullScreen(false);
			
			// Create scene
			scene = new Scene(mainLayout, 800, 600);
			
			// Set scene style
			scene.getStylesheets().add("application/dark-theme.css");
			
			// Show window
			window.setScene(scene);
			window.show();
			
			// Layout and control event handlers
			WorkspaceEvent.start();
			
			// Layout and control settings
			Emulation.settings();
			Browser.settings();
			TabCollections.settings();
			Console.settings();
			Resources.settings();
			WorkspaceMenu.settings();
			
			// Layout position setup
			mainLayout.setLeft(viewLayout);
			mainLayout.setRight(editorLayout);
			
			// Layout size setup
			editorLayout.prefWidthProperty().bind(window.widthProperty().multiply(0.45));
			editorLayout.prefHeightProperty().bind(window.heightProperty());
			
			// Layout add children
			editorLayout.getChildren().addAll(WorkspaceMenu.mainMenuBar, TabCollections.editorTabs, TabCollections.utilityTabs);	
			viewLayout.getChildren().addAll(Emulation.emulationBar, Browser.browser);

	}
}
